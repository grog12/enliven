<table align="center"><tr><td align="center" width="9999">
<img src="https://cdn.discordapp.com/avatars/801159808929497208/0995f1b0c7cf4bfc88698c114b3bb18c.png" align="center" alt="Project icon">

# Enliven
<a href="https://discord.com/oauth2/authorize?client_id=801159808929497208&permissions=2184571456&scope=bot">
<img src="https://img.shields.io/badge/Official bot instance:-Enliven %235399-yellow?style=for-the-badge&logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACn0lEQVQ4jW2TS0hUYRTHf9+de8cZH+NoM+PbUdMpoSAisECCIGgTQUQtJCKCaNOuVrqtaBFB0KJaRRiUy4hWlSSRC0mGDHxlar6bQR3HmbnzuPeLb8bUHgfO5nzn/L///zwENaFhoAEhEtimwAZ0F3+aBAQIAVIqV4ESYE4HWkCWI2yfYTZQbC8RM8ch4wS0LRgBWEAG9D1QVwuZtHowFMA6mlWuLcPrriLcrd0c/xgHdwr0ItAkZAX4E9DsQAyFke/6oaxSMVnXC/AalFnsC8epXx2lrz6IDGzgP7CKuz1L6eEkj6f38zB8EhYXIZUCT16O0PMMJdieHPMjIYKzXzh/+R6chtH38OohVIZgbgQI30W6y6CmCXKZfKm+LTHqprpjmuTFKc71BnFOmHweXGEhFoChRnAmoTYNtrZdzK4ugUOS2yzCiGqEOuL89PhZaOqGQyFwjYHfBqkXJrHLtN8SKE2zPlmB0XeMB16bwesz9IaD1A88hYNHYTW6lch/AJRlHWieLLQtQeMmo9EK5p+8xRybhLNnwDTBsv8B2OmBLiEOrHj4MNTMqQGb9OgscAmIqJFDOg0eL+TSfwNI0AQkXLC0QUDEudW2TE3XFHY1XItcJRXohL4X0P8J/NWFjcwDqN9NJ8JYwzjxjch4D0UbkpvBZTAjYCVpbbnAcKiE+88eMZczyWoaWNYW+ZrQLBaNmpVioqGNvZ2txI74WLtxG28WvFVA9RT4v0J7gjvzA/Q8fwm+gKr+oSRIDBtbuLgyaRM03pCYNpl1DZETToxkBu27jTENdfPFzKQjUO4t9A2kYrAOojw/D5GElTLIAlUT4MhBRgfbUfBNCU4v+Cohq5KI7ZwzJPKSVEPVsqiCrSXRFLoKCbVMu84Z5n4B/oIEalc81VUAAAAASUVORK5CYII=">
<br>
</a>

> Powerful music and logging bot for Discord  

**Fully free** [Discord](https://discord.com/) bot with Embed music playback, buttons control, various music sources (including livestreams) and more.  
The best opportunities for logging messages.
</td></tr>
<tr><td align="center" width="9999">

#### Join our support server:
[![Discord support server](https://discordapp.com/api/guilds/706098979938500708/widget.png?style=banner3)](https://discord.gg/zfBffbBbCp)

</td></tr>
</table>

## Features
 * Music player
   * Embed playback control
     * Buttons control
     * Progress display
     * Queue display
     * Request history
     * This and more in one control message:  
     <img src="https://gitlab.com/enlivenbot/enliven/-/wikis/uploads/e0810f0e0e8ca07b5bb202286181ac8d/image.png" />
   * Various music sources
     * Youtube, Twitch, SoundCloud and all [supported by youtube-dl sites](https://ytdl-org.github.io/youtube-dl/supportedsites.html)
     * Spotify
     * Yandex Music
     * Live streams
   * Bass boost support
   * Custom playlists
   * Multiple player nodes
 * Logging
   * Collects a full changes history
   * With attachment links
   * Display all message changes in one place
   * Fully configurable
   * Export changes history to image
     * Highlighting deletions and additions
     * Full support for rendering:
       * Emote
       * Custom emoji
       * Links
       * Mentions
     * [Example result (warning: *large image*)](https://cdn.discordapp.com/attachments/667271058461687809/722864116741439629/History-672479528634941440-722861553564778588.png)
 * Multilingual

## Getting started

1. You need to add the bot to your server using [this link](https://discord.com/oauth2/authorize?client_id=801159808929497208&permissions=2184571456&scope=bot)
2. To find out all available commands use `&help`

### Initial Configuration

* The default prefix for all commands is `&` (*or bot user mention*) by default. Set a custom server prefix with the `setprefix` command!  
* You can choose the language for the bot using the `language` command.  
* You can set up message logging using the `logging` command. 

## Contributing

If you'd like to contribute, please fork the repository and use a feature
branch. Pull requests are warmly welcome.

## Links

- [Project homepage](https://gitlab.com/skprochlab/nJMaLBot)
- [Repository](https://gitlab.com/skprochlab/nJMaLBot)
- [Issue tracker](https://gitlab.com/skprochlab/nJMaLBot/-/issues)
- [Discord support server](https://discord.gg/zfBffbBbCp)  
- Enliven on discord bots lists:
  - Links will appear here as soon as I regain access to my account. The official source of information is this gitlab.
  
  - [top.gg](https://top.gg/bot/801159808929497208) (as soon as approved)

  - [discordbotlist.com](https://discordbotlist.com/bots/enliven)

  - [discord.bots.gg](https://discord.bots.gg/bots/801159808929497208) 
    
  - [bots.discordlabs.org](https://bots.discordlabs.org/bot/801159808929497208) 
  
## Help us
You can help by making a contributing or by telling your friends about the bot. The more people use the bot, the better!

Also, you can upvote bot on botlists:

- [top.gg](https://top.gg/bot/801159808929497208) (as soon as approved)

- [discordbotlist.com](https://discordbotlist.com/bots/enliven)

- [bots.discordlabs.org](https://bots.discordlabs.org/bot/801159808929497208) (as soon as approved)

## Compiling sources
1. Clone this repo:
```
git clone https://gitlab.com/enlivenbot/enliven.git
```
2. Navigate to repo folder
3. Fetch all submodules:
```
git submodule update --init --recursive
```
4. Install .NetCore 3.1
5. Compile project:
```
dotnet build
```

## Self-hosting
⚠️ **Are you sure you want to self-host your bot?** ⚠️  
The version we host has no restrictions. Consider using it.

If you still want to continue - please ***use the self-hosted version for personal use only***. Don't promote it. Be understanding.

Also, we are not responsible for the code and its operability, updatability, backward compatibility, etc. For all questions - write to our support server.

1. Compile the sources using the [Getting started section](https://gitlab.com/enliven/bot/enliven#getting-started).
2. Install, launch and configure [lavalink](https://github.com/Frederikam/Lavalink#server-configuration)
3. Navigate to binaries folder (typically `Enliven/bin/Release/dotnetcore3.1/`) and launch the bot (`./Enliven` for linux or `Enliven.exe` for Windows)
4. After first launch bot would generate config file (`Config/config.json`). Edit it.
### Self-hosting FAQ
#### How to add lavalink nodes to bot
Edit `LavalinkNodes` variable in `Config/config.json` file:
```json
"LavalinkNodes": [
    {
    "RestUri": "http://localhost:8081",
    "WebSocketUri": "ws://localhost:8081",
    "Password": "mypass",
    "Name": "Name will be displayed in player embed"
    },
    {
    "RestUri": "http://localhost:8082",
    "WebSocketUri": "ws://localhost:8083",
    "Password": "mypass",
    "Name": null
    }
]
```
#### How to change text on the embeds
1. Edit localization files in `Common/Localization/`. (Consider using document search to find what you want to change)
2. Build project: `dotnet build` (need to be executed in repository root)
#### How to update bot
1. Update repo:
```
git pull
git submodule update --recursive
```
2. Build project:
```
   dotnet build
```