﻿namespace Bot.Utilities.ObservableDictionaryUtils {
    /// <summary>
    /// Specifies the mode of scanning through the tree
    /// </summary>
    public enum TraversalMode {
        InOrder = 0,
        PostOrder,
        PreOrder
    }
}